/// <reference path="../typings/globals/jquery/index.d.ts" />
var cart = {}; //корзина

$.getJSON('goods.json', function(data){
    //debugger;
    var goods = data;
    console.log(goods);
    checkCart();
    showCart();

    function showCart(){
        var out = '';
        for(var key in cart){
            out += '<button class="delete" data-art="' + key + '">x</button>';
            out += '<img src="' + goods[key].image + '" width="48">';
            out += goods[key].name;
            out += '<button class="minus" data-art="' + key + '">-</button>';
            out += cart[key];
            out += '<button class="plus" data-art="' + key + '">+</button>';
            out += cart[key]*goods[key].cost;
            out += '<br>';
        }
        $('#my-cart').html(out);
        $('.plus').on('click',plusGoods);
        $('.minus').on('click',minusGoods);
        $('.delete').on('click',deleteGoods);
    }

    function plusGoods(){
        var articul = $(this).attr('data-art');
        cart[articul]++;
        saveCartToLS();//save cart to localstorage
        showCart();
    }
    
    function minusGoods(){
        var articul = $(this).attr('data-art');
        if(cart[articul]>1) cart[articul]--; 
        saveCartToLS();//save cart to localstorage
        showCart();
    }

    function deleteGoods(){
        var articul = $(this).attr('data-art');
        delete cart[articul];
        saveCartToLS();//save cart to localstorage
        showCart();
    }
});

function checkCart(){
    //проверяю наличие корзины в localstorage
    if( localStorage.getItem('cart') != null && localStorage.getItem('cart') != undefined){
        cart = JSON.parse(localStorage.getItem('cart'));
    }
}

function saveCartToLS(){
    localStorage.setItem( 'cart', JSON.stringify(cart) );   
}