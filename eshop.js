/// <reference path="../typings/globals/jquery/index.d.ts" />

var cart = {}; //массив id и count товара

$('document').ready(function(){
    loadGoods();
    checkCart();
    showMiniCart();
});

function loadGoods(){
    $.getJSON('goods.json', function(data){
        //console.log(data);

        var out = '';
        for(var key in data){
            out += "<div class='single-goods'>";
            out += "<h3>" + data[key]['name'] + '</h3>';
            out += "<p>Цена: " + data[key]['cost'] + '</p>';
            out += "<img src='" + data[key].image + "'>";
            out += "<button class='add-to-cart' data-art='" + key + "'>Купить</button>";
            out += "</div>"; 
        }
        $('#goods').html(out);
        $('button.add-to-cart').on('click',addToCart);
    });    
}

function addToCart(){
    //добавляем товар в корзину
    var articul = $(this).attr('data-art');

    if( cart[articul] != undefined ){
        cart[articul]++;
    }else{
        cart[articul] = 1;
    }
    localStorage.setItem( 'cart', JSON.stringify(cart) );   
    showMiniCart();
}

function checkCart(){
    //проверяю наличие корзины в localstorage
    if( localStorage.getItem('cart') != null && localStorage.getItem('cart') != undefined){
        cart = JSON.parse(localStorage.getItem('cart'));
    }
}

function showMiniCart(){
    //показыаю содержание корзины
    var out = '';
    for(var w in cart){
        out += w + ' --- ' + cart[w] + '<br>';
    }
    $('#mini-cart').html(out);
}